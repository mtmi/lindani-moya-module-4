// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';


void main() {
  runApp(const MaterialApp(
    home: MyApp(),     
  ));
}




class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
     //Apply a theme to control the look and feel of the app
           theme: ThemeData(
                primarySwatch: Colors.lightGreen,
                brightness: Brightness.light, //Dark theme is enabled here
                primaryColor: Colors.lightGreen[800],
                fontFamily: 'Roboto',

                textTheme: const TextTheme(
                headline1: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                headline6: TextStyle(fontSize: 20.0, fontStyle: FontStyle.normal),
                bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Roboto'),
    ),       
      ),
         // home: const MyHomePage(title: 'Intelligent Farm'),  
         home: SplashScreen (
         seconds: 8,
         navigateAfterSeconds:MyHomePage(title: 'Intelligent Farm'),
        title:  Text(
          'SplashScreen Example',
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20.0,
              color: Colors.white),
        ),
        backgroundColor: Colors.lightBlue[200]
         ),          
    );    
  } 
}




//Home Page 

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.         
      
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
                   
          children: const <Widget>[
                        
            Padding(
              padding:  EdgeInsets.all(8.10),
              child: Text('Welcome to Intelligent Farm:',),
            ),
                      
            //Add a login form 
            LoginForm(),            
               
          ],


        ),
         
      ),
     
      // This trailing comma makes auto-formatting nicer for build methods.
    );

    
  }
}









// Create a Form widget.
class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);
  @override
  LoginFormState createState() {
    return LoginFormState();
  }
}
// Create a corresponding State class.
// This class holds data related to the form.

class LoginFormState extends State<LoginForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(58,0,58,20),
            child: TextFormField( 
              textAlign: TextAlign.center,
              decoration: InputDecoration(
              fillColor: Color.fromARGB(255, 191, 211, 226), filled: true,
              hintText: 'Enter a Username',
            ),
               
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter your username';
                }
                return null;
              },
            ),
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(58,0,58,0),
            child: TextFormField( 
              textAlign: TextAlign.center,
              decoration: InputDecoration(
              fillColor: Color.fromARGB(255, 191, 211, 226), filled: true,
              
              hintText: 'Enter a Password',
            ),
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter your password ';
                }
                return null;
              },
            ),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: ElevatedButton(
              onPressed: () {
                // Validate returns true if the form is valid, or false otherwise.
                if (_formKey.currentState!.validate()) {
                  // If the form is valid, display a snackbar. In the real world,
                  // you'd often call a server or save the information in a database.
                    ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Login Successful')),              
                  );
                 // If form validates open the next page,Dashboard 
                    Navigator.push(
                    context,
                  MaterialPageRoute(builder: (context) => const Dashboard()),);
                }
              },            
              child: const Text('login'),
            ),
          ),
        ],
      ),
    );
  }
}

// Dashboard Route - First Page 
class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
      ),
      
      body: Center(
          child: ElevatedButton(
          child: const Text('Add New Farm Animal Details'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const AnimalDetails()),
            );
          
          },
        ),),
        
        floatingActionButton: FloatingActionButton(
             onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const AnimalDetails()),
            ); },
             tooltip: 'Add New Animal',
        child: const Icon(Icons.add),
      ),
    );
  }
}

//Animal Details Route Widget 
class AnimalDetails extends StatelessWidget {
  const AnimalDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add New Farm Animal'),
      ),
      body: Center(
       ),
        
        floatingActionButton: FloatingActionButton(
             onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const AddNewAnimalDetails()),
            ); },
             tooltip: 'Add Farm Animal Profile',
        child: const Icon(Icons.add),
      ),
    );
  }
}


//Animal Details Route Widget 
class AddNewAnimalDetails extends StatelessWidget {
  const AddNewAnimalDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Farm Animals Details'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const ProfileEdit())
              );
          },
          child: const Text('Edit User Profile'),
        ),),
        
        floatingActionButton: FloatingActionButton(
             onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const AddNewAnimalDetails()),
            ); },
             tooltip: 'Add Farm Animal Profile',
        child: const Icon(Icons.add),
      ),
    );
  }
}


//Profile Route Widget 
class ProfileEdit extends StatelessWidget {
  const ProfileEdit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile Edit'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text('Save profile and Go back! to Dashboard'),
        ),
       
      ),floatingActionButton: FloatingActionButton(
             onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const Dashboard()),
            ); },
             tooltip: 'Save and Go back to Dashboard',
        child: const Icon(Icons.arrow_back),
      ),
    );
  }
}


